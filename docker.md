- 安装mysql
  - 新建E:\docker\mysql\conf和E:\docker\mysql\data
  - docker run -d --name mysql5.7 -p 3307:3306 --privileged=true --restart=always -v E:\docker\mysql\conf:/etc/mysql/conf.d -v E:\docker\mysql\data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 mysql:5.7
  - docker run -p 3307:3306 --restart=always --name=mediawiki_mysql -e MYSQL_DATABASE=wikidb -e MYSQL_USER=wikiuser -e MYSQL_PASSWORD=mysecret -e MYSQL_ROOT_PASSWORD=123456 -v E:\docker\mysql\conf:/etc/mysql/conf.d -v E:\docker\mysql\data:/var/lib/mysql -d mysql:5.7
  
- 安装rocketmq

  - 新建E:\docker\data\conf和E:\docker\data\store和E:\docker\data\logs

  - 新建E:\docker\data\docker-compose.yml

  - ```yaml
    version: '2'
    services:
      namesrv:
        image: rocketmqinc/rocketmq
        container_name: rmqnamesrv
        restart: always    
        ports:
          - 9876:9876
        volumes:
          - ./logs:/home/rocketmq/logs
          - ./store:/home/rocketmq/store
        command: sh mqnamesrv
      broker:
        image: rocketmqinc/rocketmq
        container_name: rmqbroker
        restart: always    
        ports:
          - 10909:10909
          - 10911:10911
          - 10912:10912
        volumes:
          - ./logs:/home/rocketmq/logs
          - ./store:/home/rocketmq/store
          - ./conf/broker.conf:/opt/rocketmq-4.4.0/conf/broker.conf
        #command: sh mqbroker -n namesrv:9876
        command: sh mqbroker -n namesrv:9876 -c ../conf/broker.conf
        depends_on:
          - namesrv
        environment:
          - JAVA_HOME=/usr/lib/jvm/jre
      console:
        image: styletang/rocketmq-console-ng
        container_name: rocketmq-console-ng
        restart: no    
        ports:
          - 8076:8080
        depends_on:
          - namesrv
        environment:
          - JAVA_OPTS= -Dlogging.level.root=info   -Drocketmq.namesrv.addr=localhost:9876 
          - Dcom.rocketmq.sendMessageWithVIPChannel=false
      mysql:
        image: mysql5.7
        container_name: mysql5.7
        restart: always    
        ports:
          - 3306:3306
        volumes:
          - ./mysql/data:/var/lib/mysql 
          - ./mysql/conf:/etc/mysql/conf.d
        environment:
          - MYSQL_ROOT_PASSWORD=123456
    ```
    
  - 新建E:\docker\data\conf\broker.conf

    ```conf
    brokerName = broker-a  
    brokerId = 0  
    deleteWhen = 04  
    fileReservedTime = 48  
    brokerRole = ASYNC_MASTER  
    flushDiskType = ASYNC_FLUSH  
    # 如果是本地程序调用云主机 mq，这个需要设置成 云主机 IP
    brokerIP1=192.168.10.180
    ```
  
  - 进入E:\docker\data目录 cmd命令 
  
  - 执行docker-compose up -d 



- 安装nginx 

  - 新建E:\docker\nginx \conf和E:\docker\nginx\www和E:\docker\nginx\logs

  - 新建E:\docker\nginx \conf\nginx.conf

  - ```
    
    user  nginx;
    worker_processes  auto;
    
    error_log  /var/log/nginx/error.log notice;
    pid        /var/run/nginx.pid;
    
    
    events {
        worker_connections  1024;
    }
    
    
    http {
        include       /etc/nginx/mime.types;
        default_type  application/octet-stream;
    
        log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                          '$status $body_bytes_sent "$http_referer" '
                          '"$http_user_agent" "$http_x_forwarded_for"';
    
        access_log  /var/log/nginx/access.log  main;
    
        sendfile        on;
        #tcp_nopush     on;
    
        keepalive_timeout  65;
    
        #gzip  on;
    
        include /etc/nginx/conf.d/*.conf;
    }
    ```

  - 新建E:\docker\nginx \www\index.html

  - ```
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
        <h1>hello docker nginx！！！</h1>
    </body>
    </html>
    
    ```

  - 执行

  -  ```
docker run -d -p 80:80 --name nginx-server -v E:\docker\nginx\www:/usr/share/nginx/html -v E:\docker\nginx\conf\nginx.conf:/etc/nginx/nginx.conf -v E:\docker\nginx\logs:/var/log/nginx nginx



1、Docker 命令修改

docker [container](https://so.csdn.net/so/search?q=container&spm=1001.2101.3001.7020) update --restart=always 容器名字

docker [container](https://so.csdn.net/so/search?q=container&spm=1001.2101.3001.7020) update --restart=no 容器名字
